# UNIX/Linux operating systems (Basic).
1. [Installation of the OS](#part-1-installation-of-the-os) 
2. [Creating a user](#part-2-creating-a-user)
3. [Setting up the OS network](#part-3-setting-up-the-os-network)
4. [OS Update](#part-4-os-update)  
5. [Using the sudo command](#part-5-using-the-sudo-command)  
6. [Installing and configuring the time service](#part-6-installing-and-configuring-the-time-service)  
7. [Installing and using text editors](#part-7-installing-and-using-text-editors)  
8. [Installing and basic setup of SSHD service](#part-8-installing-and-basic-setup-of-the-sshd-service)  
9. [Installing and using the top, htop utilities](#part-9-installing-and-using-the-top-htop-utilities)   
10. [Using the fdisk utility](#part-10-using-the-fdisk-utility)   
11. [Using the df utility](#part-11-using-the-df-utility)    
12. [Using the du utility](#part-12-using-the-du-utility)    
13. [Installing and using the ncdu utility](#part-13-installing-and-using-the-ncdu-utility)    
14. [Working with system logs](#part-14-working-with-system-logs)     
15. [Using the CRON job scheduler](#part-15-using-the-cron-job-scheduler)

## Part 1. Installation of the OS
#### 1.1. Part 1. Install Ubuntu 20.04 Server LTS without GUI
![linux](src/images/Part_1.png "Подсказка")

## Part 2. Creating a user
#### 2.1. Part 2. Creating a user can read logs from var_log
     $sudo useradd -G adm -s /bin/bash -m user_1
     $sudo passwd user_1 -> Задаем пароль
![user_1](src/images/Part_2.png "Подсказка")

## Part 3. Setting up the OS network
#### 3.1. Change name on machine to user-1:
     $ sudo nano /etc/hostname -> rewrite hostname to new name of machine.
     $ sudo nano /etc/hosts -> rewrite hosts file
     $ sudo reboot -> reboot system
     $ hostname -> check name of name machine
#### 3.2. Set the time zone:
     $ timedatectl -> watch timezone whitch was set
     $ ls -l /etc/localtime -> Or can check in file timezone
     $ cat /etc/timezone -> this file also contain timezone
     $ timedatectl list-timezones -> check list of all timezones
     $ sudo timedatectl set-timezone Europe/Moscow -> Set new timezone
![Europe/Moscow](src/images/timedatctl.png "Europe/Moscow")
#### 3.3. Output the names of the network interfaces using a console command:
     $ ip link -> all list of names (don't show ip adress)
     $ ip a -> show names of network interfaces and ip adress
     lo (loopback inerface) is local interface, which was created by
        operating system. Only machine (computer) can see this interface.
        used for example for debugging web pages.
#### 3.4. Use the console command to get the ip address:
     $ ip r -> get dynamical ip adress from DHCP
     DHCP - dynamic host Configuraion protocol.
#### 3.5. External ip address (ip) and internal IP address:
     $ ip route, & ip route | grep default -> internal IP address;
     $ ip addr
#### 3.6. Set static ip, gw, dns settings and ping 8.8.8.8 + ya.ru:
     /etc/network/interfaces -> here there is settings of network
     $ ip route show match 0/0 -> check current gateway address
     $ sudo addr add 10.0.2.16/24 broadcast 10.0.2.255 dev enp0s3 -> ip-address, broadcast settings
     $ sudo ip route add default via 10.0.2.2
     $ ping 8.8.8.8
![ping_8.8.8.8](src/images/ping_8.8.8.8.png)
     $ ping ya.ru
![ping_ya](src/images/ping_ya.png)
## Part 4. OS Update
#### 4.1. Update the system packages to the latest version:
     $ sudo apt-get update -> updating information about packages
     $ sudo apt-get upgrade -> updating packages || also can use $sudo apt-get dist-upgrade
![updating-os](src/images/update_os.png "Part 4.")
## Part 5. Using the sudo command
#### 5.1. Allow user created in Part 2 to execute sudo command:
     sudo is do command from right of root and need passwod of root.
     $ sudo usermod -aG sudo user_1 -> give root right to user_1
     $ su - user_1 -> Now we are user_1 (switch user)
     $ sudo nano /etc/hostname 
     $ sudo nano /etc/hosts
     $ sudo reboot
     $ hostname
![change_hostname_from_user_1](src/images/Part_5.png "change_hostname_from_user_1")
## Part 6. Installing and configuring the time service
#### 6.1. Set up the automatic time synchronisation service:
     $ sudo apt-get install ntp -> install packages
     $ sudo apt install systemd-timesyncd -> install NTP
     $ sudo gedit /etc/ntp.conf: (change servers for sichronisation)
          server ru.pool.ntp.org
          server pool.ntp.org
          server time.nist.gov
          server ntp.psn.ru
          server ntp1.imvp.ru
     $ timedatectl set-ntp true -> activate NTP
     $ systemctl unmask systemd-timesyncd.service -> service is masked which means it can’t be started -> do unmask
     $ sudo service ntp restart -> restart service
     $ timedatectl show
![synchronised-time](src/images/synchronised.png "synchronised-time") \
links sources: [unmasked systemd](https://www.ubuntututorials.org/failed-to-set-ntp-not-supported/), 
[short instraction](https://itshaman.ru/articles/257/sinkhronizatsiya-vremeni-cherez-internet-v-ubuntu)
## Part 7. Installing and using text editors
#### 7.1. Write your nickname in VIM, NANO, MCEDIT:
     ----NANO----
     $ sudo nano test_nano.txt
     $ ctrl + x -> y -> enter - save changes in file when we in NANO
     ----VIM----
     $ sudo vim test_vim.txt
     $ esc -> shift + : -> wq - save changes in file when we in VIM
     ----MCEDIT----
     $ sudo mcedit test_mcedit.txt
     $ esc or 'Fn' + 'F10' -> save changes: yes
NANO
![nano](src/images/test_nano.png "NANO")
VIM
![VIM](src/images/test_vim.png "VIM")
MCEDIT
![MCEDIT](src/images/test_mcedit.png "MCEDIT")
#### 7.2. Close the file without saving the changes in VIM, NANO, MCEDIT:
     ----NANO----
     $ ctrl + x -> n -> enter - don't save changes in file when we in NANO
     ----VIM----
     $ esc -> shift + : -> q! - don't save changes in file when we in VIM
     ----MCEDIT----
     $ esc or 'Fn' + 'F10' -> save changes: No
NANO
![nano](src/images/test_nano_1.png "NANO")
VIM
![VIM](src/images/test_vim_1.png "VIM")
MCEDIT
![MCEDIT](src/images/test_mcedit_1.png "MCEDIT")
#### 7.3. Edit the file again (similar to the previous point) and search a word and replacing a word with any other one:
----NANO---- \
$ sudo nano test_nano.txt -> open and change it to "21 school 21" \
$ In nano press 'ctrl' + 'W' -> Search word \
![search_nano](src/images/search_nano.png) \
In nano touch 'ctrl' + '\' and write '21' for searching and press 'Enter'
![replace_nano_0](src/images/replace_nano_0.png) \
Next write new word for replacing and press 'A' for all words \
![replace_nano_1](src/images/replace_nano_1.png) \
![replace_nano_2](src/images/replace_nano_2.png)
----VIM---- \
$ sudo vim test_vim.txt -> open and change it to "21 school 21" \
$ In vim touch 'shift' + ':' and write \21 and press 'Enter' -> search word. Press 'n' go to the next word \
![search_vim](src/images/search_vim.png) \
$ In vim touch 'shift' + ':' and write %s/21/69/g and press 'Enter' -> search and replace \
  % -> the range from the first to the last line of the file \
  g -> replace all occurrences of the search pattern in the current line \
![search_replace_vim](src/images/search_replace_vim.png) \
----MCEDIT---- \
$ sudo mcedit test_mcedit.txt -> open and change it to "21 school 21" \
Search -> 'Fn' + 'F7' \
![search_mcedut](src/images/search_mcedit.png)
Replace -> 'Fn' + 'F4' -> write what searching and word of replacing and press 'Enter' \
![search_replace_mcedut](src/images/search_replace_mcedut.png)
![search_replace_mcedut_1](src/images/search_replace_mcedut_1.png)
## Part 8. Installing and basic setup of the SSHD service
#### 8.1. Install the SSHd service:
     $ sudo apt update
     $ sudo apt install openssh-server
     $ sudo systemctl status ssh -> check SSH work
![openssh-server](src/images/install_openssh.png)
#### 8.2. Add an auto-start of the service whenever the system boots:
     $ sudo systemctl enabale ssh
     $ ssh localhost -> check the functionality of the utility
#### 8.3. Reset the SSHd service to port 2022:
     $ sudo nano /etc/ssh/sshd_config -> Do settins in the configuration file
![port_2022](src/images/port_2022.png)
#### 8.4. Show the presence of the sshd process using the ps command:
     $ sudo systemctl status ssh -> find PID (идентификатор процесса)
     $ ps -p 1953 -> if know PID of process we can write it using key -p
![process](src/images/process.png)
#### 8.5. reboot system and do netstat -tan:
     $ sudo reboot -> reboot system
     $ sudo apt install net-tools -> install net-tools
     $ netstat -tan -> мониторинг сетей TCP / IP, который может отображать таблицы маршрутизации, фактические сетевые подключения и информацию о состоянии каждого устройства сетевого интерфейса.
       -t -> Состояние соединения по протоколу передачи;
       -a -> показать все соединения;
       -n -> использовать IP-адрес напрямую, а не через сервер доменных имен.
     Значение каждого столбца
          Proto: протокол, используемый сокетом
          Recv-Q: количество байтов, не скопированных пользовательской программой, подключенной к этому сокету.
          Send-Q: количество неподтвержденных байтов удаленного хоста
          Локальный адрес: локальный адрес (имя локального хоста) и номер порта сокета. 
          Внешний адрес: удаленный адрес (имя удаленного хоста) и номер порта сокета.
          State: состояние сокета.
     Форма «0.0.0.0» — это стандартный способ сказать «нет конкретного адреса», все адреса IPv4 на локальном компьютере.
![netstat.png](src/images/netstat.png)
## Part 9. Installing and using the top, htop utilities
#### 9.1. From the output of the top command determine and write in the report:
     $ sudo apt-get update
     $ sudo apt-get install top
     $ sudo apt-get install htop
     $ top -> run top
     uptime -> 2:23
     number of authorised users -> 1 user
     total system load -> load average: 0.00, 0.00, 0.00
     total number of processes -> Tasks: 93 total
     cpu load -> 100 - id = 100 - 99.7 = 0.3 %
     memory load -> 1458.3 + 1739.0 = 3197.3
     pid of the process with the highest memory usage -> PID = 656 Определили по %MEM
     pid of the process taking the most CPU time -> PID = 1059 Определили по TIME+
#### 9.2. Add a screenshot of the htop command output to the report:
Sort_pid
![sort-pid](src/images/sort_pid.png)
Sort_percent_cpu
![sort-cpu](src/images/sort_cpu.png)
Sort_percent_mem
![sort_mem](src/images/sort_mem.png)
Sort_time
![sort_time](src/images/sort_time.png)
Filter_process_sshd
![filter_process_sshd](src/images/filter_process_sshd.png)
Search syslog process
![Search_syslog](src/images/search_syslog.png)
Add_infor_bar
![Infor_bar](src/images/add_infor_bar.png)
## Part 10. Using the fdisk utility
#### 10.1. Run the fdisk -l command:
     $ sudo fdisk -l | less
       name: /dev/sda;
       capacity: 10 GiB;
       number of sectors: 20971520.
## Part 11. Using the df utility
     $ df
       partition size: 9336140 kilobytes
       space used: 4535884 kilobytes
       space free: 4306280 kilobytes
       percentage used: 52%
     $ df -Th
       partition size: 9.0 Gigabyte
       space used: 4.4 Gigabyte
       space free: 4.2 Gigabyte
       percentage used: 52%
       Type: ext4
## Part 12. Using the du utility
$ sudo du -s /home
![sudo du -s /home](src/images/du_home.png) \
$ sudo du -sh /home
![sudo du -sh /home](src/images/du_h_home.png) \
$ sudo du -s /var
![sudo du /var](src/images/du_var.png) \
$ sudo du -sh /var
![sudo du -sh /var](src/images/du_h_var.png) \
$ sudo du -s /var/log
![sudo du /var/log](src/images/du_var_log.png) \
$ sudo du -sh /var/log
![sudo du -sh /var/log](src/images/du_h_var_log.png) \
$ du -ha /var/log
![du -ha /var/log](src/images/du_h_var_log_all.png)
## Part 13. Installing and using the ncdu utility
     $ sudo apt-get update
     $ sudo apt-get install ncdu
$ sudo ncdu /home \
![sudo ncdu /home](src/images/ncdu_home_0.png) \
![sudo ncdu /home](src/images/ncdu_home_1.png) \
$ sudo ncdu /var \
![sudo ncdu /var](src/images/ncdu_var_0.png) \
![sudo ncdu /var](src/images/ncdu_var_1.png) \
$ sudo du /var/log \
![sudo ncdu /var/log](src/images/ncdu_var_log_0.png) \
![sudo ncdu /var/log](src/images/ncdu_var_log_1.png)
## Part 14. Working with system logs
     $ sudo cat /var/log/auth.log | less
       the last successful login time: Jun 16 16:02:22;
       user name: bastion;
       login method: as root (uid = 0);
     $ sudo service sshd restart -> Restart SSHd service
     $ sudo cat /var/log/syslog | less - > check last history
![syslog_sshd](src/images/syslog_sshd.png)
## Part 15. Using the CRON job scheduler
#### 15.1. run the uptime command in every 2 minutes:
$ sudo crontab -e -> write order when start service \
![crontab_uptime](src/images/crontab_uptime.png) \
$ sudo crontab -l \
![crontab_check_tasks](src/images/crontab_check_tasks.png) \
$ sudo crontab -r -> remove tasks \
$ sudo crontab -l -> show lists of tasks \
![crontab_show_tasks](src/images/crontab_show_tasks.png)